package com.paguemob.api.repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.paguemob.api.entity.Company;


@RepositoryRestResource(collectionResourceRel = "company", path = "company")
public interface CompanyRepository extends CrudRepository<Company, Long>{
	
	public List<Company> findByNameContainingIgnoreCase(@Param("name")String name);

	public List<Company> findByIndustry(@Param("industry") String industry);
	
}

package com.paguemob.api.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.paguemob.api.entity.Address;


@Repository
public interface AddressRepository extends CrudRepository<Address, String>{
	
	public Address findByZipCode(String zipCode);

}

package com.paguemob.api.gateway;

import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.paguemob.api.ws.correios.ConsultaCEP;
import com.paguemob.api.ws.correios.ConsultaCEPResponse;
import com.paguemob.api.ws.correios.ObjectFactory;

@Component
public class CorreiosGateway {

	@Autowired
	private WebServiceTemplate webServiceTemplate;
	
	public ConsultaCEPResponse findZipCode(String zipCode) {

		ObjectFactory objectFactory = new ObjectFactory();
		
		ConsultaCEP consultaCepRequest = objectFactory.createConsultaCEP();
		consultaCepRequest.setCep(zipCode);

		return ((JAXBElement<ConsultaCEPResponse>) webServiceTemplate.marshalSendAndReceive(objectFactory.createConsultaCEP(consultaCepRequest),
													new SoapActionCallback(""))).getValue();
	}

}

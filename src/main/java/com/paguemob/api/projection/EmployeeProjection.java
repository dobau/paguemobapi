package com.paguemob.api.projection;

import org.springframework.data.rest.core.config.Projection;

import com.paguemob.api.entity.Employee;

@Projection(name = "employee" , types = Employee.class)
public interface EmployeeProjection {
	Long getId();
	String getName();
}

## Getting Started

```bash
	git clone https://dobau@bitbucket.org/dobau/paguemobapi.git
	cd paguemobapi
	gradle bootRun
	
``` 

## Usage
***


### Should be able to register a Company
```
POST
http://localhost:8080/company
```

```json
{
	"name": "PagueMob",
	"address" : {
		"zipCode": "05577-200"
	},
	"streetNumber": "1",
	"telephone": "11 987862218",
	"website": "www.paguemob.net",
	"industry": "meios de pagamento"
}
```

### Should be able to return a list of Companies
```
GET
http://localhost:8080/company
```

### Should be able to return a list of Companies thats name contains the a specified value
```
GET
http://localhost:8080/company/search/findByNameContainingIgnoreCase?name=Pag
```

### Should be able to return a list of Companies that work in a specified industry
```
GET
http://localhost:8080/company/search/findByIndustry?industry=meios de pagamento
```

### Should be able to return a single Company with a specified id
```
GET
http://localhost:8080/company/1
```

### Should be able to return a list of employees that work at a specified Company
```
GET
http://localhost:8080/company/1/employees
```

### Should be able to register an Employee
```
POST
http://localhost:8080/employee
```

```json
{
	"name": "Rafael Alves",
	"cpf": "123",
	"job": "Software Developer",
	"company": "http://localhost:8080/company/1"
}
```

### Should be able to return a list of Employees
```
GET
http://localhost:8080/employee
```

### Should be able to return a list of Employees thats Job Title contains a specified word
GET
http://localhost:8080/employee

### Should be able to return a single Employee with a specified id
GET
http://localhost:8080/employee/1
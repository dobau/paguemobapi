package com.paguemob.api.repository.event;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import com.paguemob.api.entity.Address;
import com.paguemob.api.entity.Company;
import com.paguemob.api.gateway.CorreiosGateway;
import com.paguemob.api.repository.AddressRepository;
import com.paguemob.api.ws.correios.ConsultaCEPResponse;
import com.paguemob.api.ws.correios.EnderecoERP;

@Component
@RepositoryEventHandler
public class CompanyEventHandler {

	@Autowired
	private AddressRepository addressRespository;
	
	@Autowired
	private CorreiosGateway correios;
	
	@HandleBeforeSave
	@HandleBeforeCreate
	public void handlePersistCompany(Company company) {

		if (company.getAddress() == null) {
			return;
		}
		
		String zipCode = company.getAddress().getZipCode();
		
		Address address = addressRespository.findByZipCode(zipCode);
		
		if (address == null) {
			ConsultaCEPResponse response = correios.findZipCode(zipCode);
			
			if (response != null && response.getReturn() != null) {
				EnderecoERP enderecoERP = response.getReturn();
				
				address = new Address();
				address.setCity(enderecoERP.getCidade());
				address.setLinhas(Arrays.asList(enderecoERP.getComplemento(), enderecoERP.getComplemento2()));
				address.setDistrict(enderecoERP.getBairro());
				address.setState(enderecoERP.getUf());
				address.setStreet(enderecoERP.getEnd());
				address.setZipCode(enderecoERP.getCep());
				
				addressRespository.save(address);
			}
			
		}
		
		company.setAddress(address);
	}

}
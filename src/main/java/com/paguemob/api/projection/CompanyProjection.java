package com.paguemob.api.projection;

import org.springframework.data.rest.core.config.Projection;

import com.paguemob.api.entity.Company;

@Projection(name = "company" , types = Company.class)
public interface CompanyProjection {
	Long getId();
	String getName();
}

package com.paguemob.api.correios;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.paguemob.api.gateway.CorreiosGateway;
import com.paguemob.api.ws.correios.ConsultaCEPResponse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CorreiosTest {
	
	@Autowired
	private CorreiosGateway correios;
	
	@Test
	public void shouldReturnAddress() {
		ConsultaCEPResponse response = this.correios.findZipCode("05577200");
		Assert.assertTrue(response.getReturn().getEnd().contains("Raposo Tavares"));
	}	
}
